export function isSorted(array) {
  for (let i = 0; i < array.length - 1; ++i) {
    if (array[i] > array[i + 1]) return false;
  }
  return true;
}

export function shuffleArray(array) {
  for (let i = 0; i < array.length; ++i) {
    let randomIndex = Math.ceil(Math.random() * (array.length - 1));
    [array[i], array[randomIndex]] = [array[randomIndex], array[i]];
  }
}
