////////////////////////////////////////////////////////////////////////////////////
// bozo-sort.js                                                                   //
////////////////////////////////////////////////////////////////////////////////////
// An implementation of the bozosort algorithm.                                   //
////////////////////////////////////////////////////////////////////////////////////
// Runtime Complexity: O(lmfao^2)                                                 //
////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2021 Elhanan Flesch                                              //
// The MIT License (MIT)                                                          //
// Permission is hereby granted, free of charge, to any person obtaining a copy   //
// of this software and associated documentation files (the "Software"), to deal  //
// in the Software without restriction, including without limitation the rights   //
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell      //
// copies of the Software, and to permit persons to whom the Software is          //
// furnished to do so, subject to the following conditions:                       //
// The above copyright notice and this permission notice shall be included in     //
// all copies or substantial portions of the Software.                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR     //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,       //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE    //
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER         //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,  //
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN      //
// THE SOFTWARE.                                                                  //
////////////////////////////////////////////////////////////////////////////////////

import { isSorted } from './sort-utils.js';

function bozoSort(array) {
  const iterationLimit = 400000;
  let currentIteration = 0;

  while (!isSorted(array) && currentIteration < iterationLimit) {
    let firstIndex = Math.ceil(Math.random() * (array.length - 1));
    let secondIndex = Math.ceil(Math.random() * (array.length - 1));

    while (firstIndex === secondIndex) {
      secondIndex = Math.ceil(Math.random() * (array.length - 1));
    }

    [array[firstIndex], array[secondIndex]] = [array[secondIndex], array[firstIndex]];
    ++currentIteration;
  }

  console.log(`isSorted = ${isSorted(array)}`);
  console.log(`iterations: ${currentIteration}`);
}

const testArray = [];
for (let i = 0; i < 10; i++) {
  testArray.push(Math.ceil(Math.random() * 50));
}
console.log(testArray);

console.log("Sorting");
bozoSort(testArray);
console.log(testArray);
