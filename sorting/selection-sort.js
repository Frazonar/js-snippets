////////////////////////////////////////////////////////////////////////////////////
// selection-sort.js                                                              //
////////////////////////////////////////////////////////////////////////////////////
// An implementation of the selection sort algorithm.                             //
////////////////////////////////////////////////////////////////////////////////////
// Runtime Complexity: O(n^2)                                                     //
////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2021 Elhanan Flesch                                              //
// The MIT License (MIT)                                                          //
// Permission is hereby granted, free of charge, to any person obtaining a copy   //
// of this software and associated documentation files (the "Software"), to deal  //
// in the Software without restriction, including without limitation the rights   //
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell      //
// copies of the Software, and to permit persons to whom the Software is          //
// furnished to do so, subject to the following conditions:                       //
// The above copyright notice and this permission notice shall be included in     //
// all copies or substantial portions of the Software.                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR     //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,       //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE    //
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER         //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,  //
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN      //
// THE SOFTWARE.                                                                  //
////////////////////////////////////////////////////////////////////////////////////


function selectionSort(array) {
  for (let i = 0; i < array.length - 1; ++i) {
    let minIndex = i;

    for (let j = i + 1; j < array.length; ++j) {
      minIndex = array[j] < array[minIndex] ? j : minIndex;
    }

    if (i !== minIndex) {
      [array[i], array[minIndex]] = [array[minIndex], array[i]];
    }
  }
}

const testArray = [];
for (let i = 0; i < 10; i++) {
  testArray.push(Math.ceil(Math.random() * 50));
}
console.log(testArray);

console.log("Sorting");
selectionSort(testArray);
console.log(testArray);
