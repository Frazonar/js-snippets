////////////////////////////////////////////////////////////////////////////////////
// atbash.js                                                                      //
////////////////////////////////////////////////////////////////////////////////////
// An implementation of the Atbash cipher.                                        //
////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2021 Elhanan Flesch                                              //
// The MIT License (MIT)                                                          //
// Permission is hereby granted, free of charge, to any person obtaining a copy   //
// of this software and associated documentation files (the "Software"), to deal  //
// in the Software without restriction, including without limitation the rights   //
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell      //
// copies of the Software, and to permit persons to whom the Software is          //
// furnished to do so, subject to the following conditions:                       //
// The above copyright notice and this permission notice shall be included in     //
// all copies or substantial portions of the Software.                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR     //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,       //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE    //
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER         //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,  //
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN      //
// THE SOFTWARE.                                                                  //
////////////////////////////////////////////////////////////////////////////////////

const maxUTF16 = 0x10ffff;

function atbash(inString) {
  if (typeof inString !== "string") {
    console.error("Error: inString must be a string!");
    return "";
  }

  let outString = "";
  for (let i = 0; i < inString.length; ++i) {
    outString = outString.concat("", String.fromCharCode(maxUTF16 - inString.charCodeAt(i)));
  }

  return outString;
}

const inString = "Hello, world!";
const outString = atbash(inString);

console.log(`Input: ${inString}`);
console.log(`Output: ${outString}`);
