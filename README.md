# JavaScript Snippets
This repository holds a collection of miscellaneous JavaScript snippets.

## Instructions
Run `deno run [script-to-run]` (running the scripts in Node.js is possible, but it requires additional setup).
